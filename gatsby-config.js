module.exports = {
  siteMetadata: {
    title: `Gatsby One-Page Starter`,
    description: `Starter for an agency page.`,
    author: `@vit.kolesnik`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-agency`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, 
      },
    },
    {
      resolve: `gatsby-plugin-prefetch-google-fonts`,
      options: {
        fonts: [
          {
            family: `Montserrat`,
            subsets: [`latin`],
            variants: [`300`, `400`, `700`, `900`]
          },
          {
            family: `Source Sans Pro`,
            subsets: [`latin`],
            variants: [`300`, `400`, `500`, `700`]
          },
          {
            family: `Baskervville`,
            subsets: [`latin`],
            variants: [`400`]
          },
        ],
      },
    },
    `gatsby-plugin-sass`,
    `gatsby-plugin-offline`
  ],
}
