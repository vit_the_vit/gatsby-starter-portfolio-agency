import React, { useRef, useState , useEffect } from 'react'
import PropTypes from 'prop-types'


const Section = (props) => { 

  return (

    <section 
      id={ props.id }
      className={ props.className }
      >
    </section>
 )
}

export default Section

Section.propTypes = {
  id: PropTypes.string.isRequired,
  className: PropTypes.string
};

Section.defaultProps = {
  id: '',
  className: ''
};
