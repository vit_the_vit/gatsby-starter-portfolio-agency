import React, { useRef, useState } from 'react'
import { Link } from 'gatsby'
import Icon from '../components/icon.js'
import '../assets/scss/hamburger.scss'


const Menu = () => {

  const modal = useRef(null);
  const [ isMenuClosed, toggleMenu ] = useState( ( true ) );

  function handleMenuToggle() {
    modal.current.style = isMenuClosed ? `
      left: 0;
      opacity: 1;
    ` : `
      left: -2000px;
      opacity: 0;
    `;
    toggleMenu( !isMenuClosed );
  }

  const MenuItem = (props) => {
    return (
      <a
      href={ props.to }
      onClick= { handleMenuToggle }
    >
      { props.title }
    </a>
    );
  }

  return (
    <>
      <div className="toggler">
        <button onClick={ handleMenuToggle }>
          <Icon name="hamburger" />
        </button>
      </div>
      
      <div className="Menu">
        <div className="overlay" ref={ modal }>
          <nav>
            <MenuItem to="#screen-1" title="Home" /> 
            <MenuItem to="#screen-2" title="Services" /> 
            <MenuItem to="#screen-3" title="Clients" /> 
            <MenuItem to="#screen-4" title="Vision" /> 
            <MenuItem to="#screen-5" title="Contact" /> 
          </nav> 
        </div>
      </div>
    </>
  );
}

export default Menu
