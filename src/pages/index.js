import React, { useRef, useState } from 'react'
import Menu from '../components/hamburger.js'
import Icon from '../components/icon.js'
import image1 from '../images/img-front-01.jpg'
import image3 from '../images/img-front-03.jpg'
import image4 from '../images/img-front-04.jpg'
import image5 from '../images/img-front-gmap.png'
import '../assets/scss/main.scss'

const IndexPage = () => {
  
  const refScreen1 = useRef();
  const refScreen2 = useRef();
  const refScreen3 = useRef();
  const refScreen4 = useRef();
  const refScreen5 = useRef();

  return (

  <div className="App">
    <header>
      <Menu />
    </header>

    <main className="Main">

      <section 
        id="screen-1" 
        className="Main__section"
        ref={ refScreen1 }
      >
        <div className="Main__content">
          <span className="tagline">Who We Are</span>
          <h1>Nemo</h1>
          <p>We are a web development studio combining innovation, minimalistic design, and cutting-edge technology.&nbsp;
            <span className="smm"><a href="#"><Icon name="facebook" /></a> <a href="#"><Icon name="twitter" /></a> <a href="#"><Icon name="instagram" /></a></span>
          </p>
          
          <button className="arrow" 
            onClick={() => refScreen2.current.scrollIntoView({ behavior: "smooth" })}
          >
            <Icon name="arrowDown" />
          </button>
        </div>
        <div className="Main__content--filled">
          <img className="filling" src={ image1 } alt="" />
        </div>
      </section>

      <section 
        id="screen-2"
        className="Main__section"
        ref={ refScreen2 }
      >
        <div className="Main__content">
          <span className="tagline">What We Do</span>
          <h1>Services</h1>
          <p>We offer a range of services focused on web design and web development.</p>
          <button className="arrow" 
            onClick={() => refScreen3.current.scrollIntoView({ behavior: "smooth" })}
          >
            <Icon name="arrowDown" />
          </button>
        </div>
        <div 
          className="Main__content long bg-darken"
        >
        <div className="col">            
          <h2>Web Design</h2>
          <ul>
            <li>UI &#38; UX</li>
            <li>Logo &#38; branding</li>
            <li>Art Direction</li>
           </ul>
          </div>
          <div className="col">
            <h2>Web Development</h2>
            <ul>
              <li>Frontend &#38; Backend </li>
              <li>React, Angular</li>
              <li>WordPress, WooCommerce</li>
            </ul>
          </div>
        </div>
      </section>

      <section 
        id="screen-3"
        className="Main__section"
        ref={ refScreen3 }
      >
        <div className="Main__content">
          <span className="tagline">Happy</span>
          <h1>Clients</h1>
          <p>Our success is measured by results, the most important being how our clients feel about their experience with us.</p>
          <button className="arrow" 
            onClick={() => refScreen4.current.scrollIntoView({ behavior: "smooth" })}
            >
            <Icon name="arrowDown" />
          </button>
        </div>
        <div className="Main__content--filled">
        <img className="filling" src={ image3 } alt="" />
        </div>
      </section>

      <section 
        id="screen-4"
        className="Main__section"
        ref={ refScreen4 }
      >
        <div className="Main__content">
          <span className="tagline">How We See It</span>
          <h1>Vision</h1>
          <p >An effective and immersive user experience is what captures attention and sends a clear message. 
          This is why we attach great importance to a clean design as well as robust tech behind it.</p>
          <button className="arrow" 
            onClick={() => refScreen5.current.scrollIntoView({ behavior: "smooth" })}
            >
            <Icon name="arrowDown" />
          </button>
        </div>
        <div className="Main__content--filled">
          <img className="filling" src={ image4 } alt="" />
        </div>
      </section>

      <section 
        id="screen-5"
        className="Main__section"
        ref={ refScreen5 }
      >
        <div className="Main__content">
          <span className="tagline">How To Reach Us</span>
          <h1>Contact</h1>
          <ul className="contact">
            <li><Icon className="b1" name="cellphone" /><a href="tel:01.74.34.61.84">+33 01.74.34.61.84</a></li>
            <li><Icon className="b2" name="mapmarker" /><a href="tel:01.74.34.61.84">1, rue Michel Ange 17346 Strasbourg, France</a></li>
            <li><Icon className="b3" name="email" /><a href="mailto:john.doe@example.com">john.doe@example.com</a></li>
          </ul>
          <button className="arrow" 
            onClick={() => refScreen1.current.scrollIntoView({ behavior: "smooth" })}
            >
            <Icon name="arrowUp" />
          </button>
          <div className="smm"><a href="#"><Icon name="facebook" /></a> <a href="#"><Icon name="twitter" /></a> <a href="#"><Icon name="instagram" /></a></div>
        </div>
        <div className="Main__content--filled">
          <img className="filling" src={ image5 } alt="" />
        </div>
      </section>
    </main>
  </div>
  )
}

export default IndexPage

