# gatsby-starter-agency

**This is an agency portfolio starter for Gatsby.js V2.**

## Preview

https://gatsby-starter-agency.netlify.com

## Installation

Install this starter (assuming Gatsby is installed) by running from your CLI:

`gatsby new gatsby-agency https://gitlab.com/vit.kolesnik/gatsby-starter-portfolio-agency`

Run `gatsby develop` in the terminal to start the dev site.

Please note: the images are not part of the template and are used for demonstration purposes only.

### Supporting the author

Thanks for using this project! I'm always interested in seeing what people do with my projects, so don't hesitate to share your project with me.

If you want to hire me for contract/freelance work, you can do so — get in touch with me!

